const problem2 = require("../problem2");


const textFileName = 'lipsum.txt';

function callback(err, data){
    if(err){
        console.log(err);
    }else{
        console.log(data);
    }
}

problem2(textFileName, callback);
