const problem1 = require("../problem1");
const fs = require("fs");

const directory = "randomJsonFiles";

function callback(err, data){
    if(err){
        console.log(err);
    }else{
        console.log(data);
    }
}

problem1(directory, callback);
