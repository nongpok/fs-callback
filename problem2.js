const fs = require('fs');

function problem2(textFileName, callback){

    fs.readFile(textFileName, 'utf-8', function(err, data){
        if(err){
            callback(new Error(`Error in reading file ${textFileName}`), null);
        }else{

            const contentInUpperCase = data.toUpperCase();
            const nameForUpperCaseFile = 'upperCaseFile.txt';

            fs.writeFile(nameForUpperCaseFile, contentInUpperCase, function(err){
                if(err){
                    callback(new Error(`Error in creating file ${nameForUpperCaseFile}`), null);
                }else{
                    console.log(`${nameForUpperCaseFile} created`);

                    const fileNamesList = 'filenames.txt';

                    fs.writeFile(fileNamesList, nameForUpperCaseFile, function(err){
                        if(err){
                            callback(new Error(`Error writing in file ${fileNamesList}`), null);
                        }else{
                            console.log(`${nameForUpperCaseFile} added in ${fileNamesList}`);

                            fs.readFile(nameForUpperCaseFile, 'utf-8', function(err, data){
                                if(err){
                                    callback(new Error(`Error in reading ${nameForUpperCaseFile}`), null);
                                }else{

                                    const contentInLowerCase = data.toLowerCase();
                                    const splittedContent = contentInLowerCase.split(". ").join('.\n');
                                    const nameForLowerCaseFile = 'lowerCaseFile.txt';

                                    fs.writeFile(nameForLowerCaseFile, splittedContent, function(err){
                                        if(err){
                                            callback(new Error(`Error in creating file ${nameForLowerCaseFile}`), null);
                                        }else{

                                            console.log(`${nameForLowerCaseFile} created`);

                                            fs.appendFile(fileNamesList, ` ${nameForLowerCaseFile}`, function(err){
                                                if(err){
                                                    callback(new Error(`Error writing in file ${fileNamesList}`), null);
                                                }else{
                                                    console.log(`${nameForLowerCaseFile} added in ${fileNamesList}`);

                                                    fs.readFile(nameForLowerCaseFile, 'utf-8', function(err, data){
                                                        if(err){
                                                            callback(new Error(`Error in reading ${nameForUpperCaseFile}`), null);
                                                        }
                                                        else{

                                                            const sortedData = data.split('\n').sort().join('\n');
                                                            const sortedFileName = 'sortedFile.txt';

                                                            fs.writeFile(sortedFileName, sortedData, function(err){
                                                                if(err){
                                                                    callback(new Error(`Error in creating ${sortedFileName}`), null);
                                                                }
                                                                else{
                                                                    console.log(`${sortedFileName} created`);

                                                                    fs.appendFile(fileNamesList, ` ${sortedFileName}`, function(err){
                                                                        if(err){
                                                                            callback(new Error(`Error in reading ${nameForUpperCaseFile}`), null);
                                                                        }else{
                                                                            console.log(`${sortedFileName} added in ${fileNamesList}`);
                                                                            
                                                                            fs.readFile(fileNamesList, 'utf-8', function(err, data){
                                                                                if(err){
                                                                                    callback(new Error(`Error in reading ${fileNamesList}`));
                                                                                }else{

                                                                                    fileNamesListsData = data.split(' ');

                                                                                    fileNamesListsData.map(function(fileName){
                                                                                        fs.unlink(fileName, function(err){
                                                                                            if(err){
                                                                                                callback(new Error(`Error in deleting ${fileName}`))
                                                                                            }else{
                                                                                                console.log(`${fileName} deleted successfully`);
                                                                                            }
                                                                                        })
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });

                                                                }
                                                            });
                                                        }
                                                    });
                                                }   
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

module.exports = problem2;