const fs = require("fs");

function problem1(directory, callback){

    fs.mkdir(directory, function(err){
      if(err){
          callback(new Error(`Error in creating directory ${directory}`), null);
      }else{

          console.log(`${directory} directory created`);

          let totalFiles = 10;
          let fileNamePrefix = "randomJson";

          for(let fileCounter = 0; fileCounter < totalFiles; fileCounter++){

            let presentFileName = `${directory}/${fileNamePrefix}${fileCounter}.json`;

              fs.writeFile(presentFileName, JSON.stringify({'randomJsonFileIndex' : fileCounter }), function(err){
                  if(err){
                    callback(new Error('error in creating json file'), null);
                  }else{

                    console.log(`${fileName}${fileCounter}.json created in ${directory} directory`);

                    //deleting it simultaneously

                    fs.unlink(presentFileName, function(err){
                      if(err){
                        callback(new Error(`Error in deleting ${presentFileName}`), null);
                      }else{
                        console.log(`${presentFileName} successfully deleted`);
                      }
                    });

                  }
              }
            );
          }

      }
    });
}

module.exports = problem1;
